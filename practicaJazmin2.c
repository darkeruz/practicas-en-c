/*Realizar un programa en c que me permita cargar el apellido, nombre, nota1, nota2 de alumnos, para ello utilizar una estructura
 luego escribirlo en un archivo, despues leerlo, y mostrarlo por pantalla. (ingresar 3 alumnos)*/


#include <stdio.h>
#include <stdlib.h>

typedef struct{
    char apellido[20], nombre[20];
    float nota1, nota2;
    int key;
}Alumno;

int posicion(int n){
    return(n-1);
};


int main(){
    int cantidad, i=1, pos;

    Alumno alumno;

    printf("Ingrese la cantidad de alumnos que desea cargar: \n");
    scanf("%i", &cantidad);

    FILE *archivo2 = fopen("notasAlumnos.txt", "w");

    while(i < cantidad+1){
            //cargamos los datos del alumno
            alumno.key = i;
            printf("Alumno[%d] - Apellido: \n", i);
            scanf("%s", alumno.apellido);
            printf("Alumno[%d] - Nombre: \n", i);
            scanf("%s", alumno.nombre);
            printf("Alumno[%d] - Nota primer parcial: \n", i);
            scanf("%f", &alumno.nota1);
            printf("Alumno[%d] - Nota segundo parcial: \n", i);
            scanf("%f", &alumno.nota2);
            //Calculamos la posicion
            pos = posicion(alumno.key) * sizeof(Alumno);
            fseek(archivo2, pos, SEEK_SET);
            fwrite(&alumno, 1, sizeof(Alumno), archivo2);
            i++;
    }

    fclose(archivo2);



    archivo2 = fopen("notasAlumnos.txt", "r");

    if(!archivo2){
        printf("Error al intentar abrir el archivo\n");
        exit(EXIT_FAILURE);
    }

    i = 1;
    while(i < cantidad+1){
            pos = posicion(i) * sizeof(Alumno);
            fseek(archivo2, pos, SEEK_SET);
            fread(&alumno, 1, sizeof(Alumno), archivo2);

            printf("Alumno[%d] - Apellido: %s\n", i, alumno.apellido);
            printf("Alumno[%d] - Nombre: %s\n", i, alumno.nombre);
            printf("Alumno[%d] - Nota1: %.2f\n", i, alumno.nota1);
            printf("Alumno[%d] - Nota2: %.2f\n", i, alumno.nota2);
            i++;
    }

    fclose(archivo2);

    return 0;
}
