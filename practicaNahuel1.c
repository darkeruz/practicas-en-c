/*Realizar un programa en C que escriba en un archivo los numeros del 0 al 100
y luego los lea desde el archivo y los muestre por pantalla*/

#include <stdio.h>
#include <stdlib.h>


void escribir(void){
    int i=0;

    FILE *fp = fopen("archivo1.txt", "w");

    while(i<101){
        fputc(i, fp);
        i++;
    }

    fclose(fp);

};

void leer(void){
    int i;

    FILE *fp = fopen("archivo1.txt", "r");

    if(!fp){
        fprintf(stderr, "Error al intentar abrir el archivo especificado.\n");
        exit(EXIT_FAILURE);
    }

    rewind(fp);

    while(1){
        if(feof(fp)) break;
        i = fgetc(fp);
        printf("%d ", i);
    }

    fclose(fp);

};

int main(){
    escribir();
    leer();


    return 0;
}



