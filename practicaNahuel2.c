/*Realizar un programa en C que me permita ingresar el apellido, nombre, nota1, nota2, de un alumno en una estructura
    luego escribir en un archivo 3 alumnos utilizando el fwrite y por ultimo leerlos usando el fread*/

#include <stdio.h>
#include <stdlib.h>

typedef struct{
    char apellido[20], nombre[20];
    float nota1, nota2;
    int key;
}Alumno;

int posicion(int n){
    return (n-1);
};

void escribir(int cantidad){
    int i=1, pos;

    FILE * archivo2 = fopen("archivo2.bd", "w");

    Alumno alumno;

    while(i < cantidad+1){
        //Ingresamos los datos de nuestros alumnos
        alumno.key = i;
        printf("Alumno[%d] - Apellido: \n", i);
        scanf("%s", alumno.apellido);
        printf("Alumno[%d] - Nombre: \n", i);
        scanf("%s", alumno.nombre);
        printf("Alumno[%d] - Nota 1: \n", i);
        scanf("%f", &alumno.nota1);
        printf("Alumno[%d] - Nota 2: \n", i);
        scanf("%f", &alumno.nota2);

        //Calculamos la posicion donde setearemos el SEEK_SET
        pos = posicion(alumno.key) * sizeof(Alumno);
        //Setear el SEEK_SET
        fseek(archivo2, pos, SEEK_SET);
        fwrite(&alumno, 1, sizeof(Alumno), archivo2);
        i++;
    }

    fclose(archivo2);
};



void leer(int cantidad){
    int i=1, pos;

    FILE *archivo2 = fopen("archivo2.bd", "r");

    if(!archivo2){
        fprintf(stderr, "Error al intentar abrir el archivo especificado\n");
        exit(EXIT_FAILURE);
    }

    printf("\nMostrando todos los alumnos!\n");

    Alumno alumno;

    while(i < cantidad+1){
        pos = posicion(i) * sizeof(Alumno);
        fseek(archivo2, pos, SEEK_SET);
        fread(&alumno, 1, sizeof(Alumno), archivo2);

        printf("Alumno[%d] - Apellido: %s\n", i, alumno.apellido);
        printf("Alumno[%d] - Nombre: %s\n", i, alumno.nombre);
        printf("Alumno[%d] - Nota 1: %.2f\n", i, alumno.nota1);
        printf("Alumno[%d] - Nota 2: %.2f\n", i, alumno.nota2);
        i++;
    }

    fclose(archivo2);

};

int main(){
    int cantidad;

    printf("Ingrese la cantidad de alumnos a cargar: \n");
    scanf("%d", &cantidad);

    escribir(cantidad);
    leer(cantidad);

    return 0;
}
