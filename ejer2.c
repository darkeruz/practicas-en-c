//Escribe en un archivo todos los numeros primos menores a 1000 y luego los lee del mismo archivo y los muestra por pantalla.
#include <stdio.h>
#include <stdlib.h>

int esPrimo(int n);
void escribirArchivo(void);
void leerArchivo(void);

int main(){
    escribirArchivo();
    leerArchivo();

    return 0;
}

int esPrimo(int n){
    int i, primo = 1;

    for(i=2; i<n; i++){
        if(n%i == 0){
            primo = 0;
            break;
        }
    }
    return primo;
}

void escribirArchivo(void){
    FILE *fp = fopen("ejer2.txt", "w");
    int i=1;

    while(i<1000){
        if(esPrimo(i)){
            //fprintf(fp, "%i ", i);
            fwrite(&i, 1, sizeof(int), fp);
        }i++;
    }

    fclose(fp);
}

void leerArchivo(void){
    FILE *fp = fopen("ejer2.txt", "r");

    if(!fp){
        fprintf(stderr, "Error al intentar abrir el archivo especificado.\n");
        exit(EXIT_FAILURE);
    }

    printf("\n===A partir de aqui comienza a leer el archivo!===\n");

    int j;
    rewind(fp);
    while(1){
        //fscanf(fp, "%d", &j);
        fread(&j, 1, sizeof(int), fp);
        if(feof(fp)) break;
        fprintf(stdout, "%d ", j);
    }

    fclose(fp);
}
