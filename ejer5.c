#include <stdio.h>
#include <stdlib.h>

typedef struct{
    char apellido[20], nombre[20];
    int edad, key;
    float dinero;
}Cliente;

int posicion(int n){
    return (n-1);
};

Cliente agregarCliente(void){
    Cliente c;
    printf("Ingrese el apellido: \n");
    scanf("%s", c.apellido);
    printf("Ingrese el nombre: \n");
    scanf("%s", c.nombre);
    printf("Ingrese la edad: \n");
    scanf("%i", &c.edad);
    printf("Ingrese el dinero: \n");
    scanf("%f", &c.dinero);
    return c;
};

void mostrarClientes(Cliente c, int n){
    int i=1, pos;
    FILE *fp = fopen("test2.txt", "r");

    if(!fp){
        fprintf(stderr, "Error al intentar abrir el archivo especificado.\n");
        exit(EXIT_FAILURE);
    }

    puts("\n===Los clientes son:===\n");
    while(i < n+1){
            pos = posicion(i) * sizeof(Cliente);
            fseek(fp, pos, SEEK_SET);
            fread(&c, 1, sizeof(Cliente), fp);
            printf("Cliente[%d] - Apellido: %s - Nombre: %s - Edad: %i - Dinero: %.2f\n", i, c.apellido, c.nombre, c.edad, c.dinero);
            i++;
    }
    fclose(fp);
}

int main(){
    int i=1, n, pos;
    FILE *fp = fopen("test2.txt", "w");
    Cliente c;

    printf("Ingrese la cantidad de clientes: \n");
    scanf("%i", &n);

    while(i < n+1){
        c = agregarCliente();
        c.key = i;
        pos = posicion(i) * sizeof(Cliente);
        fseek(fp, pos, SEEK_SET);
        fwrite(&c, 1, sizeof(Cliente), fp);
        i++;

    }
    fclose(fp);

    mostrarClientes(c, n);





    return 0;
}
