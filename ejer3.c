// Realizar un programa en c que me permita registrar los nombres de 4 alumnos con las respectivas
// notas de 2 parciales y un recuperatorio. Realizando las siguientes funciones:
//     1. Función para cargar los alumnos en un arreglo de alumnos.
//     2. Función que imprima los nombres y las notas de todos los alumnos.
//     3. Funcion que devuelva la cantidad de alumnos que aprobaron los dos parciales.
//     4. Funcion que ordene el vector de acuerdo al nombre en forma alfabetica.

#include <stdio.h>
#include <stdlib.h>

typedef struct{
    char apellido[20], nombre[20];
    int edad,key;
    float nota1, nota2, nota_recuperatorio;

}alumno;

void cargar_alumnos(alumno alumnos);
void imprimir_todo(alumno alumnos);
void aprobaron_dos_parciales(alumno alumnos);
void ordenar_alumnos(alumno alumnos);
void clear_buffer(void);
int posicion(int key;);




int main(){
    alumno alumnos[4];

    cargar_alumnos(alumnos[4]);
    imprimir_todo(alumnos[4]);
    aprobaron_dos_parciales(alumnos[4]);
    //ordenar_alumnos(alumnos);




    return 0;
}

void cargar_alumnos(alumno alumnos){
    FILE *fp = fopen("ejer3.txt", "w+b");
    int i,pos;
    for(i=0; i<4; i++){
        alumnos.key = i+1;
        printf("Ingrese el apellido del alumno %d: ",i+1);
        scanf("%s", alumnos.apellido);
        printf("\nIngrese el nombre del alumno %d: ",i+1);
        scanf("%s", alumnos.nombre);
        printf("\nIngrese la edad del alumno %d: ",i+1);
        scanf("%d", &alumnos.edad);
        printf("\nIngrese la nota del primer parcial del alumno %d: ",i+1);
        scanf("%f",&alumnos.nota1);
        printf("\nIngrese la nota del segundo parcial del alumno %d: ",i+1);
        scanf("%f", &alumnos.nota2);
        if(alumnos.nota1 < 4 || alumnos.nota2 < 4){
            printf("\nIngrese la nota del recuperatorio del alumno %d: ",i+1);
            scanf("%f", &alumnos.nota_recuperatorio);
        }
        pos = posicion(alumnos.key)*sizeof(alumno);
        fseek(fp, pos, SEEK_SET);
        fwrite(&alumnos, 1, sizeof(alumno), fp);
    }

    fclose(fp);
}

void imprimir_todo(alumno alumnos){
    int n=1, pos;

    FILE *fp = fopen("ejer3.txt", "r");

    if(!fp){
        fprintf(stderr, "Error al intentar abrir el archivo especificado.\n");
        exit(EXIT_FAILURE);
    }


    puts("\n\n");
    printf("===Todos los alumnos con sus respectivas notas:=== \n");
    printf("Apellido - Nombre - Edad - Nota 1 - Nota 2 - Nota recuperatorio\n");

    while(n<5){
        pos = posicion(n)*sizeof(alumno);
        fseek(fp, pos, SEEK_SET);
        fread(&alumnos, 1, sizeof(alumno), fp);
        printf("%s - ", alumnos.apellido);
        printf("%s - ", alumnos.nombre);
        printf("%d - ", alumnos.edad);
        printf("%.2f - ", alumnos.nota1);
        printf("%.2f - ", alumnos.nota2);
        printf("%.2f\n", alumnos.nota_recuperatorio);
        n++;
    }


}

void aprobaron_dos_parciales(alumno alumnos){
    FILE *fp = fopen("ejer3.txt", "r");

    if(!fp){
        fprintf(stderr, "Error al intentar abrir el archivo especificado.\n");
        exit(EXIT_FAILURE);
    }

    printf("\n===Alumnos que aprobaron los dos parciales:=== \n");

    int i=1, pos=0;
    printf("\n\n");
    while(i < 5){
        pos = posicion(i)*sizeof(alumno);
        fseek(fp, pos, SEEK_SET);
        fread(&alumnos, 1, sizeof(alumno), fp);
        if(alumnos.nota1 >= 4 && alumnos.nota2 >= 4){
            printf("Apellido: %s\n", alumnos.apellido);
            printf("Nombre: %s\n", alumnos.nombre);
            printf("Nota 1: %.2f\n", alumnos.nota1);
            printf("Nota 2: %.2f\n", alumnos.nota2);
            printf("Nota recuperatorio: %.2f\n\n", alumnos.nota_recuperatorio);
        }i++;
    }

    fclose(fp);

}

//void ordenar_alumnos(alumno alumnos);

void clear_buffer(void){
    int i;
    do{
        i = getchar();
    }while(i != '\n' && i != 'EOF');
}

int posicion(int key){
    return (key-1);
}


