#include <stdio.h>
#include <stdlib.h>

void writeFile(void);
void readFile(void);

int main(){
    writeFile();
    readFile();

    return 0;
}

void writeFile(void){
    FILE *fp = fopen("ejer1.txt", "w");

    char string[80], option = '_';
    int i=0;

    while(option != 'y'){
        puts("write some text here: \n");
        fgets(string, 80, stdin);
        printf("you add %d lines in your file!\n", ++i);
        fputs(string, fp);
        printf("wanna exit? (y/n)\n");
        option = getchar(); getchar();
    }

    fclose(fp);
}

void readFile(void){
    FILE *fp = fopen("ejer1.txt", "r");

    if(!fp){
        fprintf(stderr, "Error trying open the specific file\n");
        exit(EXIT_FAILURE);
    }

    char c;

    printf("\n====Here the program starts reading the file====\n");

    while(1){
        if(feof(fp)) break;
        c = fgetc(fp);
        printf("%c", c);
    }

    fclose(fp);
}
