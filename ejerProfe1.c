
/*
 * |-------|------------------|--------------------|-------|
 * |  key  |     nombre       |      apellido      |dinero |
 * |-------|------------------|--------------------|-------|
 * |       |                  |                    |       |
 */

#include <stdio.h>

typedef struct{
	int key;
	char nombre[15],apellido[15]; //nombres de los arreglos son direcciones
	float dinero; //el nombre de una variable no es una dirección
}cliente;

int orden(int key){
	return(key-1);
};

int main(void){
	FILE * fp;
	cliente C;
	int pos=0, n=0;


	fp = fopen("clientes.txt", "w+b");


	while (n<3) {
		printf("Ingrese el clinete [%d]",++n);
		C.key=n;
		printf("  Nombre: "); scanf("%s",C.nombre);
		printf("Apellido: "); scanf("%s",C.apellido);
		printf("  Dinero: "); scanf("%f",&C.dinero);
		pos=orden(C.key)*sizeof(cliente);
		fseek(fp,pos,SEEK_SET);
		fwrite(&C,1,sizeof(cliente),fp);
	}


	n=1;
	puts("\n\n");
	printf("%15s%15s%10s\n","Nombre","Apellido","Dinero");

	while(n<4){
		pos=orden(n)*sizeof(cliente);
		fseek(fp,pos,SEEK_SET);
		fread(&C,1,sizeof(cliente),fp);
		printf("%15s",C.nombre);
		printf("%15s",C.apellido);
		printf("%8.2f\n",C.dinero);
		n++;
	}

	fclose(fp);
	return 0;
}
