#include <stdio.h>
#include <stdlib.h>

int main(){
    char cadena[20], opcion = 'z', c;
    int i=0;

    FILE *archivo = fopen("archivo1.txt", "w+");

    if(!archivo){
        printf("El archivo no existe.\n");
        exit(EXIT_FAILURE);
    }

    while(1){
        printf("escribe una cadena de texto\n");
        fgets(cadena, 20, stdin);
        printf("Se agrego %d cadena de texto\n", ++i);
        fputs(cadena, archivo);
        printf("quieres salir? (s/n)\n");
        opcion = getchar(); getchar();
        if(opcion == 's' || opcion == 'S') break;

    }

    printf("\nAqui comienza a leer el archivo\n");
    rewind(archivo);

    while(1){
        if(feof(archivo)){
          break;
        }
        c = fgetc(archivo);
        printf("%c", c);
    }


    fclose(archivo);

    return 0;
}
