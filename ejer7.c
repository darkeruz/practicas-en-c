  /*Una inmobiliaria de la provincia de la rioja necesita almacenar los datos de las propiedades que tienen para alquilar
 y vender por lo tanto se necesita registrar los sig datos de cada propiedad.
 - Identificador de la propiedad (alfanumerico)
 - Nombre del propietario (Alfanumerico)
 - Valor de la propiedad (mayor a cero)
 - Tipo de propiedad (Casa,departamento,otros)
 - Tipo de oferta (Venta - alquiler)
 Se debera definir una estructura en c que permita guardar 100 propiedades con todos los atributos requeridos.

 1.- Funcion de carga de las propiedades.
 2.- Desarrollar una funcion que me imprima todas propiedad (Nombre del propietario y precio)
 3.-Desarrollar una funcion que me permita buscar una propiedad por su codigo identificador y devolver la propiedad si la busqueda es exitosa.
 4.-Funcion que me imprima el precio y nombre del propietario de la propiedad de mayor valor.
 */

 #include <stdio.h>
 #include <stdlib.h>
 #include <string.h>


 typedef struct{
    char identificador[20], nombre_propietario[20], tipo_propiedad[20], tipo_oferta[20];
    float valor_propiedad;
    int key;
 }Propiedad;

 int posicion(int n){
    return (n-1);
 };

 void cargar_propiedad(int cantidad){
    int i=1, pos;
    Propiedad prop;  //Creamos una instancia de la estructura Propiedad llamada prop

    FILE * fp = fopen("ejer7.bd", "w"); //creamos un archivo llamado fp y con la "w" especificamos que podremos escribir en el

    while(i < cantidad+1){
        //Cargamos los datos de nuestra estructura
        prop.key = i;
        printf("Propiedad[%d] - Identificador: \n",i);
        scanf("%s", prop.identificador);
        printf("Propiedad[%d] - Nombre del propietario: \n", i);
        scanf("%s", prop.nombre_propietario);
        printf("Propiedad[%d] - Valor de la propiedad: \n", i);
        scanf("%f", &prop.valor_propiedad);
        printf("Propiedad[%d] - Tipo de propiedad: \n", i);
        scanf("%s", prop.tipo_propiedad);
        printf("Propiedad[%d] - Tipo de oferta: \n", i);
        scanf("%s", prop.tipo_oferta);

        //Calculamos la posicion donde setearemos el seek
        pos = posicion(i) * sizeof(Propiedad);
        //seteamos el seek en la posición calculada
        fseek(fp, pos, SEEK_SET);
        fwrite(&prop, 1, sizeof(Propiedad), fp);
        i++;
    }

    fclose(fp);

 };

 void imprimir_todas_propiedades(int cantidad){
     int i=1, pos;
     Propiedad prop;

     FILE *fp = fopen("ejer7.bd", "r");

     if(!fp){
        fprintf(stderr, "Error al intentar abrir el archivo especificado.\n");
        exit(EXIT_FAILURE);
     }

     puts("\nTODAS LAS PROPIEDADES!\n");
     while(i < cantidad+1){
        pos = posicion(i) * sizeof(Propiedad);
        fseek(fp, pos, SEEK_SET);
        fread(&prop, 1, sizeof(Propiedad), fp);
        printf("Propiedad[%d] - Identificador: %s\n", i, prop.identificador);
        printf("Propiedad[%d] - Nombre del propietario: %s\n", i, prop.nombre_propietario);
        printf("Propiedad[%d] - Valor de la propiedad: %.2f\n", i, prop.valor_propiedad);
        printf("Propiedad[%d] - Tipo de propiedad: %s\n", i, prop.tipo_propiedad);
        printf("Propiedad[%d] - Tipo de oferta: %s\n\n", i, prop.tipo_oferta);
        i++;
     }

     fclose(fp);
 };

 void buscar_propiedad_por_codigo(int cantidad, char codigo[20]){
     int i=1, pos, flag=1;
     Propiedad prop;

     FILE *fp = fopen("ejer7.bd", "r");

     if(!fp){
        fprintf(stderr, "Error al intentar abrir el archivo especificado\n");
        exit(EXIT_FAILURE);
     }

     puts("\nBUSCANDO PROPIEDAD: \n");
     while(i < cantidad+1){
        pos = posicion(i) * sizeof(Propiedad);
        fseek(fp, pos, SEEK_SET);
        fread(&prop, 1, sizeof(Propiedad), fp);
        if(strcmp(prop.identificador, codigo) == 0){
            printf("Propiedad[%d] - Identificador: %s\n", i, prop.identificador);
            printf("Propiedad[%d] - Nombre del propietario: %s\n", i, prop.nombre_propietario);
            printf("Propiedad[%d] - Valor de la propiedad: %.2f\n", i, prop.valor_propiedad);
            printf("Propiedad[%d] - Tipo de propiedad: %s\n", i, prop.tipo_propiedad);
            printf("Propiedad[%d] - Tipo de oferta: %s\n", i, prop.tipo_oferta);
            flag = 0;
            break;
        }
        i++;
     }

     if(flag) printf("El codigo ingresado no corresponde a ninguna propiedad.\n");

     fclose(fp);

 };

 void propiedad_mayor_valor(int cantidad){
     int i=1, pos, indice;
     float mayor_precio;
     Propiedad prop;

     FILE *fp = fopen("ejer7.bd", "r");

     if(!fp){
        fprintf(stderr, "Error al intentar abrir el archivo especificado\n");
        exit(EXIT_FAILURE);
     }

     puts("\nPROPIEDAD MAS CARA: \n");
     while(i < cantidad+1){
        pos = posicion(i) * sizeof(Propiedad);
        fseek(fp, pos, SEEK_SET);
        fread(&prop, 1, sizeof(Propiedad), fp);
        if(prop.valor_propiedad > mayor_precio || i == 1){
            mayor_precio = prop.valor_propiedad;
            indice = prop.key;

        }
        i++;
     }

     pos = posicion(indice) * sizeof(Propiedad);
     fseek(fp, pos, SEEK_SET);
     fread(&prop, 1, sizeof(Propiedad), fp);
     printf("Propiedad[%d] - precio: %.2f - Nombre del propietario: %s\n", prop.key, prop.valor_propiedad, prop.nombre_propietario);


     fclose(fp);
 };

 int main(){
    int cantidad;
    char codigo[20];
    printf("Ingrese la cantidad de propiedades que desea cargar: \n");
    scanf("%d", &cantidad);

    cargar_propiedad(cantidad);
    imprimir_todas_propiedades(cantidad);

    printf("Ingrese el código identificador de la propiedad que desea  buscar: \n");
    scanf("%s", codigo);
    buscar_propiedad_por_codigo(cantidad, codigo);

    propiedad_mayor_valor(cantidad);

    return 0;
 }


