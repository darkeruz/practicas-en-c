/*4.- Confeccione un programa que genere un archivo de base de dato que
permita dar de agregar, borrar , cambiar y listar por registros los campos
(apellido, nombre, edad, curso ) de un listado de alumnos. El acceso a los
registros debe ser de manera aleatoria a partir de un menú de selección
iterativo. Queda a su criterio definir el modo de obtener el campo Key .
→ 6 puntos
(4 punto extra) Definir el campo key con codigo de depto. (dos caracteres)
codigo de carrera (dos caracteres) N.o de orden de inscripción (4 caracteres)*/

#include <stdio.h>
#include <stdlib.h>

typedef struct{
    char codigo_depto[2], codigo_carrera[2], numero_orden[4];

}Key;

typedef struct{
    Key keyAlumno;
	int key;
	char apellido[20], nombre[20], curso[20];
	int edad;
}Alumno;


int orden(int key){
	return(sizeof(Alumno)*(key-1));
};

char menu(void); //Funcion Menu
int agregar_alumno(FILE *, int); //Funcion para agregar un alumno al archivo
void borrar_alumno(FILE *, int); //Funcion para "borrar" un alumno del archivo
void cambiar_datos_alumno(FILE *,int);//Funcion para cambiar los datos de un alumno
void mostrar_alumnos(FILE *,int);//Funcion que mostrará todos los alumnos

int main(void){
	FILE * fp;
	int cantidad_alumnos=0;	//cuenta la cantidad de alumnos ingresados
	char c='0';		//control de ciclo

	fp = fopen("alumnos.bd", "w+b"); //abre el archivo

	while(c!='5') {
		switch(menu()){
			case '1':cantidad_alumnos = agregar_alumno(fp,cantidad_alumnos);
					printf("\nSe ingresaron: %d alumnos\n",cantidad_alumnos);
					break ;
			case '2': borrar_alumno(fp, cantidad_alumnos);break;
			case '3': cambiar_datos_alumno(fp, cantidad_alumnos);break ;
			case '4': mostrar_alumnos(fp, cantidad_alumnos); getchar();break ;
			case '5': puts("Fin"); c='5'; break;
			default : printf ( " La opcion que introdujo es erronea.\n" ); getchar();break ;
		}getchar();
		system("clear");
	}
	fclose(fp); //cierra y guarda el stream en disco como archivo
	return 0;
}


char menu(void){
	puts("=====MENU=====\n");
	puts("1. Agregar alumno.");
	puts("2. Borrar alumno.");
	puts("3. Cambiar datos del alumno.");
	puts("4. Mostrar todos los alumnos.");
	puts("5. Salir\n");
	return getchar();
}

// AGREGA UN REGISTRO AL STREAM
int agregar_alumno(FILE *fp, int cant){
		Alumno alumno; //registro auxiliar


		int pos;	// variable para calcular posición en el stream
		alumno.key=++cant; // cantidad de registros es la key
		printf("Apellido: "); scanf("%s",alumno.apellido);
		printf("Nombre: "); scanf("%s",alumno.nombre);
		printf("Edad: "); scanf("%d",&alumno.edad);
		getchar();
		printf("Curso: "); fgets(alumno.curso, 20, stdin);

		printf("Ingrese los dos primeros caracteres del nombre del departamento: \n");
		scanf("%s", alumno.keyAlumno.codigo_depto);
		printf("Ingrese las iniciales del nombre de la carrera (dos caracteres): \n");
		scanf("%s", alumno.keyAlumno.codigo_carrera);
		printf("Ingrese el numero de orden de inscripcion (4 caracteres): \n");
		scanf("%s", alumno.keyAlumno.numero_orden);


		pos=orden(alumno.key);
		fseek(fp,pos,SEEK_SET);
		fwrite(&alumno,1,sizeof(Alumno),fp);
		printf("Alumno ingresado correctamente. (presione una tecla para continuar)");
		getchar();
		return cant;
}


void borrar_alumno(FILE *fp, int cant){
	int n, pos;
	Alumno alumno;
	puts("Ingresar el ID del alumno eliminar...");
	scanf("%d",&n);
	if(n<=0 || n>cant){
		puts("El ID ingresado es incorrecto...(presione una tecla para continuar)");
		getchar();
	}else{
		pos=orden(n);
		fseek(fp,pos,SEEK_SET);
		fread(&alumno,1,sizeof(Alumno),fp);
		printf("Apellido: %s - Nombre: %s - Edad: %d - Curso: %s\n", alumno.apellido, alumno.nombre, alumno.edad, alumno.curso);
		alumno.key=-1;
		pos=orden(n);
		fseek(fp,pos,SEEK_SET);
		fwrite(&alumno,1,sizeof(Alumno),fp);
		printf("Alumno borrado correctamente! (presione una tecla para continuar...)\n");
		getchar();
	}
}

void cambiar_datos_alumno(FILE * fp,int cant){
	int n, pos;
	Alumno alumno;
	puts("Ingresar el ID del alumno modificar...");
	scanf("%d",&n);
	if(n <= 0 || n > cant){
        printf("el ID ingresado es incorrecto... (presione una tecla para continuar)...\n");
        getchar();
	}

	else{
		pos=orden(n);
		fseek(fp,pos,SEEK_SET);
		fread(&alumno,1,sizeof(Alumno),fp);
		printf("Apellido: %s - Nombre: %s - Edad: %d - Curso: %s\n", alumno.apellido, alumno.nombre, alumno.edad, alumno.curso);
		puts("Ingrese los datos nuevos del alumno: ");
		printf("apellido: ");scanf("%s",alumno.apellido);
		printf("nombre: ");scanf("%s",alumno.nombre);
		printf("edad: ");scanf("%d",&alumno.edad);
		printf("curso: ");scanf("%s",alumno.curso);
		fseek(fp,pos,SEEK_SET);
		fwrite(&alumno,1,sizeof(Alumno),fp);
		printf("Los datos nuevos fueron ingresado correctamente! (presione una tecla para continuar...)\n");
		getchar();
	}
}

void mostrar_alumnos(FILE *fp,int cant){
	int i=1,pos;
	Alumno alumno;

	while(i < cant+1){
        pos=orden(i);
		fseek(fp,pos,SEEK_SET);
		fread(&alumno,1,sizeof(Alumno),fp);
		if(alumno.key == -1)
			puts("\nAlumno eliminado...\n");
		else{
			printf("Alumno N° [%d]\n",i);
			printf("Apellido: %s - Nombre: %s - Edad: %d - Curso: %s\n", alumno.apellido, alumno.nombre, alumno.edad, alumno.curso);
			printf("El Key del alumno (Matricula) es: [%s]\n\n", alumno.keyAlumno.codigo_depto);
		}i++;

	}


}
