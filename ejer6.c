/*se desea desarrolar un programa que me permita registrar productos de un supermercado para lo cual se deberan registrar
los sig datos:
-nombre del producto.
-stock.
-categoria del producto.(comidada,bebida).
-precio
para lo cual se debera hacer lo sig:
.-Funcion de cargar el listado del producto.
.-Funcion que me devuelva el producto con el precio mas alto.
-.Funcion que oredene el listado de producto en orden ascendente tomando como criterio el precio del producto.
-.Funcion que muestre el listado.*/

#include <stdio.h>
#include <stdlib.h>

typedef struct{
    char nombre[20], categoria[20];
    int stock;
    float precio;
}Producto;

int posicion(int n){
    return(n-1);
};

void cargarProducto(int cantidad){
    int i=1, pos;
    Producto prod;
    FILE *fp = fopen("ejer6.txt", "w");

    while(i < cantidad+1){
        //Cargamos los datos a nuestra estructura Producto
        printf("Producto[%d] - Nombre: \n", i);
        scanf("%s", prod.nombre);
        printf("Producto[%d] - Stock: \n", i);
        scanf("%d", &prod.stock);
        printf("Producto[%d] - Categoria: \n", i);
        scanf("%s", prod.categoria);
        printf("Producto[%d] - precio: \n", i);
        scanf("%f", &prod.precio);

        //Establecemos la posición donde setearemos el seek
        pos = posicion(i) * sizeof(Producto);
        //Seteamos el seek en la posición establecida
        fseek(fp, pos, SEEK_SET);
        //Escribimos nuestro producto en el archivo
        fwrite(&prod, 1, sizeof(Producto), fp);
        i++;
    }

    fclose(fp);
};

void productoMasCaro(int cantidad){
    int i=1, pos;
    float precioMayor;
    char productoMasCaro[20];
    Producto prod;

    FILE *fp = fopen("ejer6.txt", "r");

    if(!fp){
        fprintf(stderr, "Error al intentar abrir el archivo especificado.\n");
        exit(EXIT_FAILURE);
    }

    puts("\n===Producto mas caro===\n");

    while(i < cantidad+1){
        //Establecemos la posición donde setearemos el seek
        pos = posicion(i) * sizeof(Producto);
        //Seteamos el seek en la posición establecida
        fseek(fp, pos, SEEK_SET);
        //Leemos el archivo y ponemos las condiciones necesarias para obtener el resultado buscado...
        fread(&prod, 1, sizeof(Producto), fp);
        if(prod.precio > precioMayor || i == 1){
            precioMayor = prod.precio;
            strcpy(productoMasCaro, prod.nombre);
        }
        i++;
    }

    printf("El producto mas caro es: %s - $%.2f\n", productoMasCaro, precioMayor);
    fclose(fp);
};
//void ordenarListado(void);
void mostrarTodo(int cantidad){
    int i=1, pos;
    Producto prod;

    FILE *fp = fopen("ejer6.txt", "r");

    if(!fp){
        fprintf(stderr, "Error al intentar abrir el archivo.\n");
        exit(EXIT_FAILURE);
    }

    puts("\n===Mostrando Todos los productos===\n");

    while(i < cantidad+1){
        //Establecemos la posicion donde setearemos el seek
        pos = posicion(i) * sizeof(Producto);
        //Seteamos el seek en la posición establecida...
        fseek(fp, pos, SEEK_SET);
        //Leemos el archivo y luego lo mostramos por pantalla
        fread(&prod, 1, sizeof(Producto), fp);
        printf("Producto[%d] - Nombre: %s - Stock: %d - Categoria: %s - Precio: %.2f\n", i, prod.nombre, prod.stock, prod.categoria, prod.precio);
        i++;
    }

    fclose(fp);
};

int main(){
    int cantidad;
    printf("Ingrese la cantidad de productos que desea agregar al listado: \n");
    scanf("%d", &cantidad);

    cargarProducto(cantidad);
    productoMasCaro(cantidad);
   // ordenarListado();
    mostrarTodo(cantidad);

    return 0;
}
