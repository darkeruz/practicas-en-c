/* Realizar un programa un c que guarde en un archivo los numeros desde el 0 hasta el 100 inclusive. y despues leer
desde el archivo y mostrarlos por pantalla.*/


#include <stdio.h>
#include <stdlib.h>


int main(){
    int i;

    FILE *archivo = fopen("archivo.txt", "w");   //w , r , w+

    for(i=0; i<101; i++){
        fprintf(archivo, "%d - ", i);  //String   Stream  stderr - stdin - stdout -
    }

    fclose(archivo);

    archivo = fopen("archivo.txt", "r");

    if(!archivo){
        printf("Error al intentar abrir el archivo especificado\n");
        exit(EXIT_FAILURE);
    }

    char c;

    rewind(archivo);

    while(1){
        if(feof(archivo)){
          break;
        }
        c = fgetc(archivo);
        printf("%c", c);

    }

    fclose(archivo);


    return 0;
}
