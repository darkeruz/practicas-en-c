#include <stdio.h>
#include <stdlib.h>

int main(){
    int vector1[5], vector2[5], vectorResultado[5], i;
    int *puntero1, *puntero2;

    puntero1 = vector1;
    puntero2 = vector2;

    for(i=0; i<5; i++){
        printf("\nIngrese un numero entero para la posicion [%d] del vector 1: ", i+1);
        scanf("%d", &vector1[i]);
    }

    printf("\tVector 1 cargado exitosamente!!!\n\n");

    for(i=0; i<5; i++){
        printf("\nIngrese un numero entero para la posicion [%d] del vector 2: ", i+1);
        scanf("%d", &vector2[i]);
        vectorResultado[i] = *(puntero1+i) + *(puntero2+i);
        }

    printf("\tVector 2 cargado exitosamente!!!\n\n");

    printf("El vector resultante es: \n");

    for(i=0; i<5; i++){
        printf("VectorResultante[%d] = %d\n",i+1, vectorResultado[i]);
    }




    return 0;
}
