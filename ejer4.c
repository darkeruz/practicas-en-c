/* Realizar un programa en C que me permita registrar el stock de un almacen atravez de las siguientes funciones:
 1.-Funcion de cargar que me permita ingresar el codigo,nombre,cantidad y precio unitario de 3 productos.
 2.-Funcion que imprima el nombre de los productos cuyo stock sea inferior a 5.
 3.-Funcion que devuelva el precio del producto cuyo codigo es pasado como parametro.
 4.-Funcion que ordene los productos y los imprima segun el codigo de menor a mayor.
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct{
    char codigo[20], nombre[20];
    int cantidad, key;
    float precio_unitario;

}Producto;

void clearBuffer(void){
    int c;
    do{
        c = getchar();
    }while(c != '\n' && c != 'EOF');
}

int posicion(int key){
        return (key-1);
};

void escribirArchivo(void){
    int n = 1,pos;
    Producto prod;

    FILE *fp = fopen("ejer4.txt", "w");

    while(n < 4){
        prod.key = n;
        printf("Ingrese el codigo del producto [%d]: \n", n);
        scanf("%s", prod.codigo);
        clearBuffer();
        printf("Ingrese el nombre del producto [%d]: \n", n);
        scanf("%s", prod.nombre);
        printf("Ingrese la cantidad del producto [%d]: \n", n);
        scanf("%d", &prod.cantidad);
        printf("Ingrese el precio unitario del producto [%d]: \n", n);
        scanf("%f", &prod.precio_unitario);
        pos = posicion(prod.key) * sizeof(Producto);
        fseek(fp, pos, SEEK_SET);
        fwrite(&prod, 1, sizeof(Producto), fp);
        n++;
    }

    fclose(fp);
};

void leerArchivo(void){
    int n = 1, pos;
    Producto prod;

    FILE *fp = fopen("ejer4.txt", "r");

    if(!fp){
        fprintf(stderr, "Error al intentar abrir el archivo especificado.\n");
        exit(EXIT_FAILURE);
    }

    puts("\n\n");

    while(n < 4){
        pos = posicion(n) * sizeof(Producto);
        fseek(fp, pos, SEEK_SET);
        fread(&prod, 1, sizeof(Producto), fp);
        printf("Producto [%d]: Codigo: %s - Nombre: %s - Cantidad: %d - Precio Unitario: $%.2f\n", prod.key, prod.codigo, prod.nombre, prod.cantidad, prod.precio_unitario);
        n++;
    }

    fclose(fp);
};

void nombreStock_inferiorA5(void){
    int n=1, pos, flag = 0;
    Producto prod;
    FILE *fp = fopen("ejer4.txt", "r");

    if(!fp){
        fprintf(stderr, "Error al intentar abrir el archivo especificado.\n");
        exit(EXIT_FAILURE);
    }

    puts("\n=====Productos cuyo stock es inferior a 5:=====\n");

    while(n < 4){
        pos = posicion(n) * sizeof(Producto);
        fseek(fp, pos, SEEK_SET);
        fread(&prod, 1, sizeof(Producto), fp);
        if(prod.cantidad < 5){
            printf("Producto id [%d] - Nombre: %s\n", prod.key, prod.nombre);
            flag = 1;
        }
        n++;
    }

    if(!flag) printf("No hay productos cuyo stock sea inferior a 5\n");

    fclose(fp);
};

int main(){
    escribirArchivo();
    leerArchivo();
    nombreStock_inferiorA5();

    return 0;
}



