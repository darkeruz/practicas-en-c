#include <stdio.h>
#include <stdlib.h>

void escribirArchivo(void){
    char cadena[80], opcion = '_';
    int i=0;

    FILE *archivo = fopen("practicaArchivos1.txt", "w");

    while(opcion != 'y'){
            printf("Ingresa alguna cadena de texto menor a 80 caracteres: \n");
            fgets(cadena, 80, stdin);
            printf("Ingresaste %d cadena de texto en total.\n", ++i);
            fputs(cadena, archivo);
            printf("Quieres terminar? - (y/n)\n");
            scanf("%c", &opcion);
            getchar();
    }

    fclose(archivo);

};


void leerArchivo(void){
    char c;
    FILE *archivo = fopen("practicaArchivos1.txt", "r");

    if(!archivo){
        fprintf(stderr, "Error, archivo no encontrado!\n");
        exit(EXIT_FAILURE);
    }

    while(1){
        if(feof(archivo)) break;
        c = fgetc(archivo);
        printf("%c", c);
    }

    fclose(archivo);
};

int main(){

    escribirArchivo();
    leerArchivo();

    return 0;
}


